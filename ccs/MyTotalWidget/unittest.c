#include <gtk/gtk.h>
#include "MyTotalWidget.h"

int 
main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *mtw;
  
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  
  gtk_window_set_title (GTK_WINDOW (window), "Aspect Frame");
  
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
                      GTK_SIGNAL_FUNC (gtk_exit), NULL);
  
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);

  mtw = mytotalwidget_new();
  gtk_container_add (GTK_CONTAINER (window), mtw);
  gtk_widget_show (mtw);

  /* And attach to its "mycounter" signal */
//  gtk_signal_connect (GTK_OBJECT (mc), "mycounter",
//                      GTK_SIGNAL_FUNC (win), NULL);

  gtk_widget_show (window);
  
  gtk_main ();
  
  return 0;
}
