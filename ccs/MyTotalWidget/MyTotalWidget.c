#include <gtk/gtkspinbutton.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkcalendar.h>
#include "MyTotal.h"
#include "MyTotalWidget.h"
#include "MyCounter.h"
#include "MyTouchscreen.h"

#define DEBUG 1

enum {
	RECALCULATE_SIGNAL,
	LAST_SIGNAL
};


static void mytotalwidget_class_init          (MyTotalWidgetClass *klass);
static void mytotalwidget_init                (MyTotalWidget      *mtw);
static void mytotalwidget_destroy             (GtkObject     *object);
static void mytotalwidget_recalculate(gpointer *widget, MyTotalWidget *mtw);

static gint mytotalwidget_signals[LAST_SIGNAL] = { 0 };


	GType
mytotalwidget_get_type (void)
{
	static GType mtw_type = 0;

	if (!mtw_type)
	{
		static const GTypeInfo mtw_info =
		{
			sizeof (MyTotalWidgetClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) mytotalwidget_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (MyTotalWidget),
			0,
			(GInstanceInitFunc) mytotalwidget_init,
		};

		mtw_type = g_type_register_static (GTK_TYPE_SPIN_BUTTON, "MyTotalWidget", &mtw_info, 0);
	}

	return mtw_type;
}

	static void
mytotalwidget_class_init (MyTotalWidgetClass *klass)
{

	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = (GtkObjectClass*) klass;
	widget_class = (GtkWidgetClass*) klass;

	object_class->destroy = mytotalwidget_destroy;
	mytotalwidget_signals[RECALCULATE_SIGNAL] = g_signal_new ("recalculate",
			G_TYPE_FROM_CLASS (klass),
			G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			G_STRUCT_OFFSET (MyTotalWidgetClass, mytotalwidget),
			NULL,
			NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);

	g_type_class_add_private (object_class, sizeof (MyTotalWidgetPrivate));
}

static void mytotalwidget_destroy (GtkObject *object)
{
	MyTotalWidget *mtw = MYTOTALWIDGET(object);
	MyTotalWidgetPrivate *priv = MYTOTALWIDGET_GET_PRIVATE(mtw);

	g_list_free(priv->linkto);
}

static void mytotalwidget_init (MyTotalWidget *mtw)
{
	MyTotalWidgetPrivate *priv = MYTOTALWIDGET_GET_PRIVATE(mtw);
	gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(mtw), (GtkAdjustment*)gtk_adjustment_new (0.0, 0.0, 10000.0, 0.0, 0.0, 0.0));
	gtk_spin_button_set_digits(GTK_SPIN_BUTTON(mtw), 2);
	gtk_entry_set_alignment(GTK_ENTRY(mtw), 1.0);

	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(mtw), TRUE);
	gtk_entry_set_width_chars(GTK_ENTRY(mtw), 6);
	gtk_editable_set_editable(GTK_EDITABLE(mtw), FALSE);

	priv->linkto = NULL;

	priv->start = NULL;
	priv->end = NULL;

	priv->calendar_multiply = NULL;
}

GtkWidget* mytotalwidget_new ()
{
	return GTK_WIDGET (g_object_new (mytotalwidget_get_type (), NULL));
}

void mytotalwidget_calendar_multiplyer(MyTotalWidget *mtw, GtkCalendar *start, GtkCalendar *end, MyCounter *mc) {
	if (start && end) {
		MyTotalWidgetPrivate *priv = MYTOTALWIDGET_GET_PRIVATE(mtw);
		priv->start = start;
		priv->end = end;

		g_object_ref(start);
		g_object_ref(end);

		g_signal_connect (G_OBJECT (priv->start), "day-selected", G_CALLBACK (mytotalwidget_recalculate), mtw);
		g_signal_connect (G_OBJECT (priv->end), "day-selected", G_CALLBACK (mytotalwidget_recalculate), mtw);
		g_signal_connect (G_OBJECT (priv->start), "month-changed", G_CALLBACK (mytotalwidget_recalculate), mtw);
		g_signal_connect (G_OBJECT (priv->end), "month-changed", G_CALLBACK (mytotalwidget_recalculate), mtw);

		priv->calendar_multiply = g_list_prepend(priv->calendar_multiply, mc);
	}
}

void mytotalwidget_add_links(MyTotalWidget *mtw, GtkWidget *mt, MyTouchscreen *mts, gchar **linkto, GtkWidget *start, GtkWidget *end) {
	guint amount = g_strv_length(linkto);
	MyTotalWidgetPrivate *priv = MYTOTALWIDGET_GET_PRIVATE(mtw);
	if (amount > 0) {
		guint i;
		GtkWidget *mc = NULL;
		for (i = 0; i < amount; i++) {
			if (mc && i > 0 && g_ascii_strcasecmp (linkto[i], "Calendar" ) == 0) {
				mytotalwidget_calendar_multiplyer(MYTOTALWIDGET(mtw), GTK_CALENDAR(start), GTK_CALENDAR(end), MYCOUNTER(mc));

				g_debug("%s", linkto[i]);
			} else {
				mc = mytouchscreen_get_counter(mts, linkto[i]);
				if (mc == NULL) mc = mytotal_get_counter(MYTOTAL(mt), linkto[i]);

				if (mc != NULL) {
					priv->linkto = g_list_prepend(priv->linkto, mc);
					g_signal_connect (G_OBJECT (mc), "recalculate", G_CALLBACK (mytotalwidget_recalculate), mtw);
				} else {
					g_warning("%s was not found to be linked.", linkto[i]);
				}
			}
		}
	} else {
		gtk_editable_set_editable(GTK_EDITABLE(mtw), TRUE);
		g_signal_connect(G_OBJECT(mtw), "value-changed", G_CALLBACK (mytotalwidget_recalculate), mtw);
	}
	g_debug("%d", g_list_length(priv->linkto));
}

static void mytotalwidget_recalculate(gpointer *widget, MyTotalWidget *mtw) {
	MyTotalWidgetPrivate *priv = MYTOTALWIDGET_GET_PRIVATE(mtw);
	gdouble total = 0.0;
	gint multiplyer = 1;
	GList *walker = g_list_first(priv->linkto);

	if (walker != NULL) {
		if (priv->start && priv->end) {
			GDate *start, *end;
			guint year, month, day;
			gtk_calendar_get_date(priv->start, &year, &month, &day);
			month++; /* Nice ;) */
			start = g_date_new_dmy(day, month, year);
			gtk_calendar_get_date(priv->end, &year, &month, &day);
			month++; /* Nice ;) */
			end = g_date_new_dmy(day, month, year);
			multiplyer = g_date_days_between(start, end);
		}


		do {
			gdouble element = 0.0;
			if (IS_MYCOUNTER(walker->data)) element = mycounter_get_total(MYCOUNTER(walker->data));
			if (IS_MYTOTALWIDGET(walker->data)) element = mytotalwidget_get_total(MYTOTALWIDGET(walker->data));

			if (g_list_find(priv->calendar_multiply, walker->data) != NULL) {
				total += (element * multiplyer);
			} else {
				total += element;
			}
			walker = g_list_next(walker);
		} while(walker != NULL);

		gtk_spin_button_set_value(GTK_SPIN_BUTTON(mtw), total);
	}


	g_signal_emit (G_OBJECT (mtw), mytotalwidget_signals[RECALCULATE_SIGNAL], 0);
}

gdouble mytotalwidget_get_total(MyTotalWidget *mtw) {
	return gtk_spin_button_get_value(GTK_SPIN_BUTTON(mtw));
}
