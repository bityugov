#ifndef __MYTOTALWIDGET_H__
#define __MYTOTALWIDGET_H__

#include <gtk/gtkspinbutton.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkcalendar.h>

#include "MyTouchscreen.h"
#include "MyTotal.h"
#include "MyCounter.h"

G_BEGIN_DECLS

#define MYTOTALWIDGET_TYPE            (mytotalwidget_get_type ())
#define MYTOTALWIDGET(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MYTOTALWIDGET_TYPE, MyTotalWidget))
#define MYTOTALWIDGET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MYTOTALWIDGET_TYPE, MyTotalWidgetClass))
#define IS_MYTOTALWIDGET(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MYTOTALWIDGET_TYPE))
#define IS_MYTOTALWIDGET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MYTOTALWIDGET_TYPE))
#define MYTOTALWIDGET_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MYTOTALWIDGET_TYPE, MyTotalWidgetPrivate))

typedef struct _MyTotalWidget        MyTotalWidget;
typedef struct _MyTotalWidgetClass   MyTotalWidgetClass;
typedef struct _MyTotalWidgetPrivate MyTotalWidgetPrivate;

struct _MyTotalWidget
{
	GtkSpinButton sb;
};

struct _MyTotalWidgetClass
{
	GtkSpinButtonClass parent_class;

	void (* mytotalwidget) (MyTotalWidget *mtw);
};

struct _MyTotalWidgetPrivate
{
	GList *calendar_multiply;
	GList *linkto;
	GtkCalendar *start, *end;
};



GType          mytotalwidget_get_type        (void);
GtkWidget*     mytotalwidget_new             (void);
void	       mytotalwidget_add_links(MyTotalWidget *mtw, 
                                       GtkWidget *mt,
				       MyTouchscreen *mts,
				       gchar **linkto,
				       GtkWidget *start,
				       GtkWidget *end);

void mytotalwidget_calendar_multiplyer(MyTotalWidget *mtw, GtkCalendar *start, GtkCalendar *end, MyCounter *mc);

gdouble	       mytotalwidget_get_total(MyTotalWidget *mtw);

G_END_DECLS

#endif /* __MYTOTALWIDGET_H__ */
