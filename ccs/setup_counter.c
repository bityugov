/* Cash Counter System
 * setup_counter.c: Reads counters from config and creates them
 * on screen in the GUI.
 * Copyright (C) 2007 Stepan Bityugov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation
 *
 */


#include <gtk/gtk.h>
#include "ccs.h"
#include "MyCounter.h"
#include "MyTotal.h"
#include "MyTouchscreen.h"
#include "MyTotalWidget.h"

void setup_counter_pages(GtkNotebook *notebook) {
	guint i;
	gsize length;
	gchar ** countertabs = g_key_file_get_string_list(config, "global", "countertabs", &length, NULL);

	for (i = 0; i < length; i++) {
		if (g_key_file_has_group(config, countertabs[i]) == TRUE) {
			gsize maxcounters, maxtotal;
			gchar *totalgroup = g_key_file_get_string (config, countertabs[i], "counters", NULL);
			gchar *countergroup = g_key_file_get_string (config, countertabs[i], "prices", NULL);

			if (totalgroup && countergroup) {
				GtkWidget *vbox = gtk_vbox_new(TRUE, 5);
				gchar ** totalcounters = g_key_file_get_keys(config, totalgroup, &maxtotal, NULL);
				gchar ** counters = g_key_file_get_keys(config, countergroup, &maxcounters, NULL);

				if (counters != NULL) {
					guint j;
					GtkWidget *mts;
					gdouble * prices = g_malloc(sizeof(gdouble) * maxcounters);

					for (j = 0; j < maxcounters; j++) {
						/* TODO: add code to find invalid values */
						prices[j] = g_key_file_get_double(config, countergroup, counters[j], NULL);
					}

					mts = mytouchscreen_new(counters, prices);
					g_strfreev(counters);
					g_free(prices);

					gtk_container_add (GTK_CONTAINER (vbox), mts);
					gtk_widget_show (mts);

					if (totalcounters != NULL) {
						gboolean calendar;
						guint j;
						GtkWidget *mt;
						GtkWidget *hbox = gtk_hbox_new(TRUE, 5);
						GtkWidget *start = NULL, *end = NULL;

						gtk_container_add (GTK_CONTAINER (vbox), hbox);

						if (g_key_file_has_key(config, countertabs[i], "Calendar", NULL) && g_key_file_get_boolean(config, countertabs[i], "Calendar", NULL)) {
							start = gtk_calendar_new();
							end = gtk_calendar_new();

							mt = mytotal_new(totalcounters);
							gtk_container_add (GTK_CONTAINER (hbox), start);
							gtk_container_add (GTK_CONTAINER (hbox), mt);
							gtk_container_add (GTK_CONTAINER (hbox), end);
							calendar = TRUE;
							gtk_widget_show (start);
							gtk_widget_show (end);
						} else {
							mt = mytotal_new(totalcounters);
							gtk_container_add (GTK_CONTAINER (hbox), mt);
							calendar = FALSE;
						}

						gtk_widget_show (mt);
						gtk_widget_show (hbox);

						for (j = 0; j < maxtotal; j++) {
							gchar **linkto = g_key_file_get_string_list(config, totalgroup, totalcounters[j], NULL, NULL);
							GtkWidget *mtw = mytotal_get_counter(MYTOTAL(mt), totalcounters[j]);
							mytotalwidget_add_links(MYTOTALWIDGET(mtw), GTK_WIDGET(mt), MYTOUCHSCREEN(mts), linkto, start, end);

							g_strfreev(linkto);
						}
						g_strfreev(totalcounters);
					}


					gtk_widget_show (vbox);
					gtk_notebook_append_page(notebook, vbox, gtk_label_new(countertabs[i]));
				} else {
					g_warning("Because the counters failed, I did not add the tab %s", countertabs[i]);
				}

				g_free(countergroup);
				g_free(totalgroup);
			} else {
				g_warning("Because there was no counter or prices group, I skipped tab %s", countertabs[i]);
			}
		} else {
			g_warning("Because the group %s was not found, I skipped it.", countertabs[i]);
		}
	}

	g_strfreev(countertabs);
}

