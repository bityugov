#include <gtk/gtk.h>
#include <string.h>
#include "MyTotal.h"

int 
main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *mt;
  gchar ** counters = g_malloc(sizeof(gchar *)*3);
  counters[0] = g_malloc(sizeof(char) * 10);
  counters[1] = g_malloc(sizeof(char) * 10);
  counters[2] = '\0';
  strcpy(counters[0], "Camping");
  strcpy(counters[1], "Totaal");
  
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  
  gtk_window_set_title (GTK_WINDOW (window), "Aspect Frame");
  
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
                      GTK_SIGNAL_FUNC (gtk_exit), NULL);
  
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);

  mt = mytotal_new(counters);

  gtk_container_add (GTK_CONTAINER (window), mt);
  gtk_widget_show (mt);

  gtk_widget_show (window);
  
  gtk_main ();
  
  return 0;
}
