#include <gtk/gtksignal.h>
#include <gtk/gtktable.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtkspinbutton.h>
#include <gtk/gtkeditable.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkcalendar.h>
#include "MyTotal.h"
#include "MyTotalWidget.h"
#include "MyTouchscreen.h"

enum {
	MYTOTAL_SIGNAL,
	LAST_SIGNAL
};

static void mytotal_class_init          (MyTotalClass *klass);
static void mytotal_init                (MyTotal      *mt);
static void mytotal_destroy             (GtkObject *object);

GType
mytotal_get_type (void)
{
	static GType mt_type = 0;

	if (!mt_type)
	{
		static const GTypeInfo mt_info =
		{
			sizeof (MyTotalClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) mytotal_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (MyTotal),
			0,
			(GInstanceInitFunc) mytotal_init,
		};

		mt_type = g_type_register_static (GTK_TYPE_TABLE, "MyTotal", &mt_info, 0);
	}

	return mt_type;
}

static void
mytotal_class_init (MyTotalClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;

  object_class->destroy = mytotal_destroy;

  g_type_class_add_private (object_class, sizeof (MyTotalPrivate));
}

static void
mytotal_init (MyTotal *mt)
{
	MyTotalPrivate *priv = MYTOTAL_GET_PRIVATE(mt);
	priv->ht = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);

	gtk_table_set_homogeneous (GTK_TABLE (mt), TRUE);
	gtk_table_set_col_spacings(GTK_TABLE (mt), 10);
}

static void
mytotal_destroy (GtkObject *object)
{
	MyTotal *mt = MYTOTAL(object);
	MyTotalPrivate *priv = MYTOTAL_GET_PRIVATE(mt);
	g_hash_table_destroy(priv->ht); /* should free key + widget */
}

GtkWidget* mytotal_get_counter (MyTotal *mt, gchar *counter) {
	MyTotalPrivate *priv = MYTOTAL_GET_PRIVATE(mt);
        return (GtkWidget*) g_hash_table_lookup(priv->ht, (const gpointer)counter);
}

GtkWidget*
mytotal_new (gchar **counters)
{
	MyTotal *mt = g_object_new (mytotal_get_type (), NULL);
	MyTotalPrivate *priv = MYTOTAL_GET_PRIVATE(mt);
	guint amount = g_strv_length(counters);
	guint i;

	gtk_table_resize (GTK_TABLE (mt), amount, 2);

	priv->counters = g_malloc(sizeof(GtkWidget *) * amount);

	for (i = 0; i < amount; i++) {
		priv->counters[i] = GTK_WIDGET(mytotalwidget_new());
		
		g_hash_table_insert (priv->ht, g_strdup(counters[i]), priv->counters[i]);
		if (counters[i][0] != '_') { /* Not Hidden */
			GtkWidget *label = gtk_label_new(counters[i]);
			gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);

			gtk_table_attach (GTK_TABLE (mt), label, 0, 1, i, i + 1,  GTK_EXPAND|GTK_FILL, GTK_EXPAND|GTK_FILL, 5, 5);
			gtk_table_attach (GTK_TABLE (mt), GTK_WIDGET(priv->counters[i]), 1, 2, i, i + 1,  GTK_EXPAND|GTK_FILL, GTK_EXPAND|GTK_FILL, 5, 5);

			gtk_widget_show (label);
			gtk_widget_show (GTK_WIDGET(priv->counters[i]));
		}
	}

	return GTK_WIDGET(mt);
}
