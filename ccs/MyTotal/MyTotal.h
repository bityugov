#ifndef __MYTOTAL_H__
#define __MYTOTAL_H__


#include <glib.h>
#include <glib-object.h>
#include <gtk/gtktable.h>
#include <gtk/gtkcalendar.h>
#include "MyTouchscreen.h"
#include "MyTotalWidget.h"
#include "MyCounter.h"

G_BEGIN_DECLS

#define MYTOTAL_TYPE            (mytotal_get_type ())
#define MYTOTAL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MYTOTAL_TYPE, MyTotal))
#define MYTOTAL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MYTOTAL_TYPE, MyTotalClass))
#define IS_MYTOTAL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MYTOTAL_TYPE))
#define IS_MYTOTAL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MYTOTAL_TYPE))
#define MYTOTAL_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MYTOTAL_TYPE, MyTotalPrivate))

typedef struct _MyTotal        MyTotal;
typedef struct _MyTotalClass   MyTotalClass;
typedef struct _MyTotalPrivate MyTotalPrivate;

struct _MyTotal
{
  GtkTable parent;
};

struct _MyTotalClass
{
  GtkTableClass parent_class;

  void (* mytotal) (MyTotal *mt);
};

struct _MyTotalPrivate
{
  GHashTable *ht;
  GtkWidget **counters;
  GtkCalendar *start, *end;
};

GType          mytotal_get_type        (void);
GtkWidget* mytotal_new (gchar **counters);
GtkWidget* mytotal_get_counter (MyTotal *mt, gchar *counter);

G_END_DECLS

#endif /* __MYTOTAL_H__ */
