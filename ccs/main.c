/* Cash Counter System
 * main.c: Program initialization
 * Copyright (C) 2007 Stepan Bityugov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation
 *
 */

#include <glib/gstdio.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include "ccs.h"
#include "setup_counter.h"

GKeyFile *config;

int main (int argc, char *argv[])
{
	gchar *configfile = NULL;

	GtkWidget *window, *notebook;

	gtk_init (&argc, &argv);

	if (argc > 1) {
		if (g_access(argv[1], R_OK) == 0) {
			configfile = g_strdup(argv[1]);
		} 
	}

	if (!configfile) configfile = g_strjoin(NULL, g_get_prgname(), ".ini", NULL);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect (GTK_OBJECT (window), "destroy", GTK_SIGNAL_FUNC (gtk_exit), NULL);
	gtk_container_set_border_width (GTK_CONTAINER (window), 10);

	notebook = gtk_notebook_new();
	gtk_container_add (GTK_CONTAINER (window), notebook);

	config = g_key_file_new();
	if (g_key_file_load_from_file(config, configfile, G_KEY_FILE_KEEP_COMMENTS|G_KEY_FILE_KEEP_TRANSLATIONS, NULL)) {
		gchar *title = g_key_file_get_string (config, "global", "title", NULL);
		if (title == NULL) title = g_strdup("Cash Counter System");
		gtk_window_set_title (GTK_WINDOW (window), title);
		g_free(title);
		setup_counter_pages(GTK_NOTEBOOK(notebook));
//		setup_reservation_pages(GTK_NOTEBOOK(notebook));
	} else {
		g_warning("I expect a configuration file in order to setup a screenlayout.");
	}

	g_free(configfile);

	gtk_widget_show (notebook);

	gtk_widget_show (window);

	gtk_main ();

	g_key_file_free(config);
	
	return 0;
}
