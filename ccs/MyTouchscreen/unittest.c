/* Cash Counter System
 * unittest.c: Unittest for the MyTouchscreen Widget
 * Copyright (C) 2007 Stepan Bityugov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation
 *
 */

#include <gtk/gtk.h>
#include "MyCounter.h"
#include "MyTouchscreen.h"
#include "MyTotalWidget.h"
#include <string.h>

int 
main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *mt;
/*  GtkWidget *mtw; */
  gdouble * prices = g_malloc(sizeof(gdouble)*2);
  gchar ** counters = g_malloc(sizeof(gchar *)*2);
  counters[0] = g_malloc(sizeof(char) * 10);
  counters[1] = g_malloc(sizeof(char) * 10);
  strcpy(counters[0], "camping");
  strcpy(counters[1], "camper");
  prices[0] = 1.25;
  prices[1] = 2.00;
  
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  
  gtk_window_set_title (GTK_WINDOW (window), "Aspect Frame");
  
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
                      GTK_SIGNAL_FUNC (gtk_exit), NULL);
  
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);

  mt = mytouchscreen_new(counters, prices);

  gtk_container_add (GTK_CONTAINER (window), mt);
  gtk_widget_show (mt);

  /* mtw = mytotalwidget_new();
  gtk_container_add (GTK_CONTAINER (window), mtw);
  gtk_widget_show (mtw);*/

  gtk_widget_show (window);
  
  gtk_main ();
  
  return 0;
}
