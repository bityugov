/* Cash Counter System
 * mytouchscreen.c: MyTouchscreen Widget
 * Copyright (C) 2007 Stepan Bityugov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation
 *
 */

#include <gtk/gtksignal.h>
#include <gtk/gtktable.h>
#include <gtk/gtktogglebutton.h>
#include "MyTouchscreen.h"
#include "MyCounter.h"

static void mytouchscreen_class_init          (MyTouchscreenClass *klass);
static void mytouchscreen_init                (MyTouchscreen      *mt);
static void mytouchscreen_destroy             (GtkObject *object);

GType mytouchscreen_get_type (void)
{
	static GType mt_type = 0;

	if (!mt_type)
	{
		static const GTypeInfo mt_info =
		{
			sizeof (MyTouchscreenClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) mytouchscreen_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (MyTouchscreen),
			0,
			(GInstanceInitFunc) mytouchscreen_init,
		};

		mt_type = g_type_register_static (GTK_TYPE_TABLE, "MyTouchscreen", &mt_info, 0);
	}

	return mt_type;
}

static void mytouchscreen_class_init (MyTouchscreenClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;

  object_class->destroy = mytouchscreen_destroy;

  g_type_class_add_private (object_class, sizeof (MyTouchscreenPrivate));
}

static void mytouchscreen_init (MyTouchscreen *mt)
{
	MyTouchscreenPrivate *priv = MYTOUCHSCREEN_GET_PRIVATE(mt);
	priv->ht = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
	gtk_table_set_row_spacings(GTK_TABLE(mt), 5);
	gtk_table_set_col_spacings(GTK_TABLE(mt), 5);
	gtk_table_set_homogeneous (GTK_TABLE (mt), TRUE);
	
	priv->tooltips = gtk_tooltips_new();
	g_object_ref_sink (priv->tooltips);
}

static void mytouchscreen_destroy (GtkObject *object)
{
	MyTouchscreen *mt = MYTOUCHSCREEN(object);
	MyTouchscreenPrivate *priv = MYTOUCHSCREEN_GET_PRIVATE(mt);
	g_hash_table_destroy(priv->ht); /* This should free Key + Widget */
	if (priv->tooltips) {
		g_object_unref (priv->tooltips);
		priv->tooltips = NULL;
	}
}

GtkWidget* mytouchscreen_new (gchar **counters, gdouble *prices)
{
	MyTouchscreen *mt = g_object_new (mytouchscreen_get_type (), NULL);
	MyTouchscreenPrivate *priv = MYTOUCHSCREEN_GET_PRIVATE(mt);
	guint amount = g_strv_length(counters);
	guint x = (amount / 3);
	guint i = 0;
	if ((amount % 3) > 0 ) x++;

	priv->counters = g_malloc(sizeof(GtkWidget *) * amount);
	gtk_table_resize (GTK_TABLE (mt), x, 3);

	for (i = 0; i < amount; i++) {
		priv->counters[i] = mycounter_new_with_arguments(counters[i], prices[i]);
		g_hash_table_insert (priv->ht, g_strdup(counters[i]), priv->counters[i]);
		gtk_table_attach (GTK_TABLE (mt), priv->counters[i], i / 3, (i / 3) + 1, i % 3, (i % 3) + 1, GTK_EXPAND|GTK_FILL, GTK_EXPAND|GTK_FILL, 5, 5);
		gtk_widget_show (priv->counters[i]);
		gtk_tooltips_set_tip (priv->tooltips, priv->counters[i], counters[i], NULL);
	}

	gtk_tooltips_enable(priv->tooltips);

	return GTK_WIDGET(mt);
}

GtkWidget* mytouchscreen_get_counter (MyTouchscreen *mt, gchar *counter) {
	MyTouchscreenPrivate *priv = MYTOUCHSCREEN_GET_PRIVATE(mt);
	return (GtkWidget*) g_hash_table_lookup(priv->ht, (const gpointer)counter);
}
