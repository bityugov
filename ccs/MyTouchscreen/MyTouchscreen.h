#ifndef __MYTOUCHSCREEN_H__
#define __MYTOUCHSCREEN_H__


#include <glib.h>
#include <glib-object.h>
#include <gtk/gtktable.h>
#include <gtk/gtktooltips.h>

G_BEGIN_DECLS

#define MYTOUCHSCREEN_TYPE            (mytouchscreen_get_type ())
#define MYTOUCHSCREEN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MYTOUCHSCREEN_TYPE, MyTouchscreen))
#define MYTOUCHSCREEN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MYTOUCHSCREEN_TYPE, MyTouchscreenClass))
#define IS_MYTOUCHSCREEN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MYTOUCHSCREEN_TYPE))
#define IS_MYTOUCHSCREEN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MYTOUCHSCREEN_TYPE))
#define MYTOUCHSCREEN_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MYTOUCHSCREEN_TYPE, MyTouchscreenPrivate))

typedef struct _MyTouchscreen       MyTouchscreen;
typedef struct _MyTouchscreenClass  MyTouchscreenClass;
typedef struct _MyTouchscreenPrivate MyTouchscreenPrivate;

struct _MyTouchscreen
{
  GtkTable parent;
};

struct _MyTouchscreenClass
{
  GtkTableClass parent_class;

  void (* mytouchscreen) (MyTouchscreen *mt);
};

struct _MyTouchscreenPrivate
{
  GHashTable *ht;
  GtkWidget **counters;
  GtkTooltips *tooltips;
};

GType          mytouchscreen_get_type        (void);
GtkWidget* mytouchscreen_new (gchar **counters, gdouble* prices);
GtkWidget* mytouchscreen_get_counter (MyTouchscreen *mt, gchar *counter);

G_END_DECLS

#endif /* __MYTOUCHSCREEN_H__ */
