/* Cash Counter System
 * unittest.c: Unittest for the MyCounter Widget
 * Copyright (C) 2007 Stepan Bityugov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation
 *
 */

#include <gtk/gtk.h>
#include "MyCounter.h"

int main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *mc;
  
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  
  gtk_window_set_title (GTK_WINDOW (window), "Aspect Frame");
  
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
                      GTK_SIGNAL_FUNC (gtk_exit), NULL);
  
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);

  mc = mycounter_new_with_arguments ("test", 3.0);
  gtk_container_add (GTK_CONTAINER (window), mc);
  gtk_widget_show (mc);

  /* And attach to its "mycounter" signal */
//  gtk_signal_connect (GTK_OBJECT (mc), "mycounter",
//                      GTK_SIGNAL_FUNC (win), NULL);

  gtk_widget_show (window);
  
  gtk_main ();
  
  return 0;
}
