/* Cash Counter System
 * mycounter.c: MyCounter Widget
 * Copyright (C) 2007 Stepan Bityugov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation
 *
 */

#include <gtk/gtkhbox.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkspinbutton.h>
#include <glib/gstdio.h>
#include "MyCounter.h"
#include <unistd.h> // R_OK

#define DEBUG 1

enum {
	RECALCULATE_SIGNAL,
	LAST_SIGNAL
};

static void mycounter_class_init          (MyCounterClass *klass);
static void mycounter_init                (MyCounter      *mc);
static void mycounter_destroy             (GtkObject *object);

static gint mycounter_signals[LAST_SIGNAL] = { 0 };

GType mycounter_get_type (void)
{
	static GType mc_type = 0;

	if (!mc_type)
	{
		static const GTypeInfo mc_info =
		{
			sizeof (MyCounterClass),
			NULL, /* base_init */
			NULL, /* base_finalize */
			(GClassInitFunc) mycounter_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (MyCounter),
			0,
			(GInstanceInitFunc) mycounter_init,
		};

		mc_type = g_type_register_static (GTK_TYPE_HBOX, "MyCounter", &mc_info, 0);
	}

	return mc_type;
}

static void mycounter_class_init (MyCounterClass *klass)
{

	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = (GtkObjectClass*) klass;
	widget_class = (GtkWidgetClass*) klass;

	object_class->destroy = mycounter_destroy;

	mycounter_signals[RECALCULATE_SIGNAL] = g_signal_new ("recalculate",
			G_TYPE_FROM_CLASS (klass),
			G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			G_STRUCT_OFFSET (MyCounterClass, mycounter),
			NULL, 
			NULL,                
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);

	g_type_class_add_private (object_class, sizeof (MyCounterPrivate));

}

static void mycounter_destroy (GtkObject *object)
{
	MyCounter *mc = MYCOUNTER(object);
	MyCounterPrivate *priv = MYCOUNTER_GET_PRIVATE(mc);

	g_free(priv->price);
	g_free(priv->counter);
	g_free(priv->button);
	g_free(priv->name);
}

static void mycounter_recalculate(GtkWidget *button, MyCounter *mc) {
	MyCounterPrivate *priv = MYCOUNTER_GET_PRIVATE(mc);
	gint counter = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(priv->counter));
	gdouble price = gtk_spin_button_get_value(GTK_SPIN_BUTTON(priv->price));
	priv->total = price * counter;
#ifdef DEBUG
	g_debug("Total: %f", priv->total);
#endif
	g_signal_emit (G_OBJECT (mc), mycounter_signals[RECALCULATE_SIGNAL], 0);
}

static void mycounter_button_press_event (GtkWidget *button, GdkEventButton *event, MyCounter *mc) {
	MyCounterPrivate *priv = MYCOUNTER_GET_PRIVATE(mc);
	if (event->button == 1) gtk_spin_button_spin(GTK_SPIN_BUTTON(priv->counter), GTK_SPIN_USER_DEFINED, 1.0);
	if (event->button == 3) gtk_spin_button_spin(GTK_SPIN_BUTTON(priv->counter), GTK_SPIN_USER_DEFINED, -1.0);
}


static void mycounter_init (MyCounter *mc)
{
	MyCounterPrivate *priv = MYCOUNTER_GET_PRIVATE(mc);
	GtkAdjustment *spinner_adj;
	GtkWidget *vbox = gtk_vbox_new(TRUE,  2);
#ifdef ASPECT
	GtkWidget *aspect_frame = gtk_aspect_frame_new ("", 0.0, 0.0, 1.0, FALSE);
	gtk_frame_set_shadow_type(GTK_FRAME(aspect_frame), GTK_SHADOW_NONE);
#endif

	priv->total = 0;
	priv->name  = NULL;

	gtk_box_set_homogeneous(GTK_BOX(mc), FALSE);

	priv->button = gtk_button_new();
	gtk_widget_set_size_request(priv->button, 64, 64);
	gtk_widget_set_events (priv->button, GDK_BUTTON_PRESS_MASK);
	gtk_signal_connect (GTK_OBJECT (priv->button), "button_press_event", (GtkSignalFunc) mycounter_button_press_event, mc);
#ifdef ASPECT
	gtk_container_add(GTK_CONTAINER(aspect_frame), mc->button);
	gtk_box_pack_start (GTK_BOX(mc), aspect_frame, FALSE, FALSE, 0);
#else
	gtk_box_pack_start (GTK_BOX(mc), priv->button, FALSE, FALSE, 0);
#endif
	gtk_box_pack_start (GTK_BOX(mc), vbox, FALSE, FALSE, 2);

	spinner_adj = (GtkAdjustment *) gtk_adjustment_new (0.0, 0.0, 100.0, 1.0, 5.0, 5.0);
	priv->counter = gtk_spin_button_new (spinner_adj, 1.0, 0);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(priv->counter), TRUE);
	gtk_entry_set_width_chars(GTK_ENTRY(priv->counter), 2);
	gtk_entry_set_alignment(GTK_ENTRY(priv->counter), 1.0);
	gtk_signal_connect (GTK_OBJECT (priv->counter), "value-changed", (GtkSignalFunc) mycounter_recalculate, mc);
	gtk_box_pack_start (GTK_BOX(vbox), priv->counter, FALSE, FALSE, 2);


	spinner_adj = (GtkAdjustment *) gtk_adjustment_new (0.0, 0.0, 10.0, 0.01, 0.1, 0.1);
	priv->price = gtk_spin_button_new (spinner_adj, 0.01, 2);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(priv->price), TRUE);
	gtk_entry_set_width_chars(GTK_ENTRY(priv->price), 5);
	gtk_entry_set_alignment(GTK_ENTRY(priv->price), 1.0);
	gtk_signal_connect (GTK_OBJECT (priv->price), "value-changed", (GtkSignalFunc) mycounter_recalculate, mc);
	gtk_box_pack_end (GTK_BOX(vbox), priv->price, FALSE, FALSE, 2);

	gtk_widget_show_all (GTK_WIDGET(mc));

}

GtkWidget* mycounter_new ()
{
	return GTK_WIDGET (g_object_new (mycounter_get_type (), NULL));
}

void mycounter_set_name(MyCounter *mc, gchar *name) {
	MyCounterPrivate *priv = MYCOUNTER_GET_PRIVATE(mc);
	priv->name = g_strdup(name);

	if (priv->name != NULL) {
		gchar *current = g_get_current_dir();
		gchar *imagepath = g_strjoin("", current, G_DIR_SEPARATOR_S, "Images", G_DIR_SEPARATOR_S, priv->name, ".png", NULL);
		g_free(current);

		if (g_access(imagepath, R_OK) == 0) {
			GtkWidget *image = gtk_image_new_from_file(imagepath);
			if (image) gtk_button_set_image(GTK_BUTTON(priv->button), image);
		} else {
			gtk_button_set_label(GTK_BUTTON(priv->button), priv->name);
		}

		g_free(imagepath);
	}
}

void mycounter_set_price(MyCounter *mc, gdouble price) {
	MyCounterPrivate *priv = MYCOUNTER_GET_PRIVATE(mc);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(priv->price), price);
}

GtkWidget * mycounter_new_with_arguments (gchar * name, gdouble price) 
{
	GtkWidget *toreturn = mycounter_new();
	mycounter_set_name(MYCOUNTER(toreturn), name);
	mycounter_set_price(MYCOUNTER(toreturn), price);
	return toreturn;
}

gdouble mycounter_get_total(MyCounter *mc) {
	MyCounterPrivate *priv = MYCOUNTER_GET_PRIVATE(mc);
	return priv->total;
}
