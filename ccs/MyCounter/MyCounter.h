#ifndef __MYCOUNTER_H__
#define __MYCOUNTER_H__

#include <gtk/gtkhbox.h>

G_BEGIN_DECLS

#define MYCOUNTER_TYPE            (mycounter_get_type ())
#define MYCOUNTER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MYCOUNTER_TYPE, MyCounter))
#define MYCOUNTER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MYCOUNTER_TYPE, MyCounterClass))
#define IS_MYCOUNTER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MYCOUNTER_TYPE))
#define IS_MYCOUNTER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MYCOUNTER_TYPE))
#define MYCOUNTER_GET_PRIVATE(obj)  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), MYCOUNTER_TYPE, MyCounterPrivate))

typedef struct _MyCounter       MyCounter;
typedef struct _MyCounterClass  MyCounterClass;
typedef struct _MyCounterPrivate MyCounterPrivate;

struct _MyCounter
{
	GtkHBox parent;
};

struct _MyCounterClass
{
	GtkHBoxClass parent_class;

	void (* mycounter) (MyCounter *mc);
};

struct _MyCounterPrivate
{
	gchar *name;
	gdouble total;
	GtkWidget *button, *counter, *price;
};

GType          mycounter_get_type        (void);
GtkWidget *    mycounter_new             (void);
GtkWidget *    mycounter_new_with_arguments(gchar *name, gdouble price);
gdouble        mycounter_get_total(MyCounter *mc);

G_END_DECLS

#endif /* __MYCOUNTER_H__ */
